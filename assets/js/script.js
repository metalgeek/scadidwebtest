$("form").submit(function(e) {
    e.preventDefault();
    $.ajax({
      type: 'POST',
      url: 'process.php',
      data: $('form').serialize(),
      success: function() {
        window.location.href = 'index.php';
  
      },
  
    });
  });


$("#product_form").submit( function(e) {
        e.preventDefault();
                
        $.ajax({
          type: 'POST',
          url: 'form.php',
          data: $('#product_form').serialize(),
          success: function(response) {
            window.location.href = 'index.php';
          },
        });
});

  //form object for all form type validation
  
  function Form(){
    //Book input validation
      this.bookForm = '';
      this.bookForm +=    '<div class="form-group">';
      this.bookForm +=    '<label>Weight (KG)</label>';
      this.bookForm +=    '<input type="text" id="weight" name="weight" title="The weight can only consist of numbers e.g 0.5, 20" class="form-control" required><br>';
      this.bookForm +=    '<p>Please provide weight in KG format<p>';
      this.bookForm +=    '<input type="hidden" name="type" value="weight" class="form-control"><br>';
      this.bookForm +=    '<input type="hidden" name="symbol" value="Kg" class="form-control"><br>';
      this.bookForm +=    '</div>';
  
      // Disc input Validation
      this.discForm = '';
      this.discForm += '<div class="form-group">';
      this.discForm +=    '<label>Size (MB)</label>';
      this.discForm +=    '<input type="text" id="size" name="size" title="The size can only consist of numbers e.g 0.5, 20" class="form-control" required><br>';
      this.discForm +=    '<p>Please provide size in MB format<p>';
      this.discForm +=    '<input type="hidden" name="type" value="Size" class="form-control" ><br>';
      this.discForm +=    '<input type="hidden" name="symbol" value="MB" class="form-control" ><br>';
      this.discForm += '</div>';
      
    //Furniture input validation
      this.furnitureForm = '';
      this.furnitureForm += '<div class="form-group">';
      this.furnitureForm +=    '<label>Height (CM)</label>';
      this.furnitureForm +=    '<input type="text" id="height" name="height" title="The height can only consist of numbers e.g 0.5, 20" class="form-control" required=>';
      this.furnitureForm += '</div>';
  
      this.furnitureForm += '<div class="form-group">';
      this.furnitureForm +=    '<label>Width (CM)</label>';
      this.furnitureForm +=    '<input type="text" id="width" name="width" title="The width can only consist of numbers e.g 0.5, 20" class="form-control" required>';
      this.furnitureForm += '</div>';
  
      this.furnitureForm += '<div class="form-group">';
      this.furnitureForm +=    '<label>Length (CM)</label>';
      this.furnitureForm +=    '<input type="text" id="length" name="length" title="The length can only consist of numbers e.g 0.5, 20" class="form-control" required>';
      this.furnitureForm +=     '<p>Please provide dimensions in HxWxL format<p>';
      this.furnitureForm += '</div>';

      this.furnitureForm += '<div class="form-group">';
      this.furnitureForm +=    '<input type="hidden" name="type" value="Dimension" class="form-control" ><br>';
      this.furnitureForm +=     '<input type="hidden" name="symbol" value="" class="form-control" ><br>';
      this.furnitureForm += '</div>';
  
      
  
      this.Book = function(){
          return this.bookForm;
      }
  
      this.Disc = function(){
        return this.discForm
      }
  
      this.Furniture = function(){
        return this.furnitureForm;
      }

  }
  
  function selectChange(){
    var category = document.getElementById('productType').value;
    var form = new Form()
    var output = form[category]();
    document.getElementById('productForm').innerHTML = output;
   } 

  function addPageRedirect(){
    window.location.href = "addproduct.php";
  }
  
  function IndexPageRedirect(){
    window.location.href = "index.php";
  }