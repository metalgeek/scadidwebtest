<?php

use TaskApp\ProductDisplayClasses\ProductClass as ProductClass;
use TaskApp\DiscClasses\DiscProperty as DiscProperty;
use TaskApp\BookClasses\BookProperty as BookProperty;
use TaskApp\FurnitureClasses\FurnitureProperty as FurnitureProperty;


require_once __DIR__ . '/vendor/autoload.php';
?>

<?php


// interface Types{
// 	public function Tproperties();

// }

// 	class Size implements Types{
	
// 	public function Tproperties(){
// 		return array('Size');
		
// 	}
// 	}
	
// 	class Weight implements Types{

// 	public function Tproperties(){
// 		return array('Weight');
// 	}
// 	}
	
// 	class Dimension implements Types{

// 	public function Tproperties(){
// 		return array('Dimension');
// 	}
// 	}
	
	
	
// 	function describe (Types $types){
// 	echo '<strong>'. get_class($types) .':</strong>' ;
// 	if(is_array($types->Tproperties())){
// 		foreach($types->Tproperties() as $Tproperties){
// 			echo $Tproperties;
// 		}
// 	}
// 	}

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="./styles/style.css">
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    <title>Junior Dev. Task</title>
</head>
<body>
<form id="products">
    <header>
    <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h3>Products</h3>
                </div>
                <div class="col-md-6 button" >
                    <button class="btn btn-success" type="button" name="ADD" onclick="addPageRedirect()">ADD</button>
                    <button class="btn btn-danger" type="submit" name="MASS DELETE" id="delete-product-btn">MASS DELETE</button>
                </div>
            </div>
            <hr>
    </div>
    </header>
	
    <div class="col-xs-12 col-md-12 container">

			<div class="row">

				<?php
				
				$objProductClass = new ProductClass();
				$data = $objProductClass-> getProducts();
				while ($result = mysqli_fetch_array($data)) {

				?>

				<div class="col-md-3">
					<div class="card">
						<div class="card-select">
							<input type="checkbox" class="delete-checkbox" name="delete[]" value="<?php echo $result['id'] ?>">
						</div>
						<div class="card-block">
							
							<p class="card-text"><strong><?php echo $result['sku'] ?></strong></p>
							<p class="card-text"><strong><?php echo $result['name'] ?></strong></p>
							<p class="card-text"><strong>Price: <?php echo $result['price'] ?>$</strong></p>
							<p class="card-text" ><strong><?php	echo $result['type'].':'. $result['value'].$result['symbol'];?></strong></p>

						</div>
					</div>		
				</div>

				<?php

					}

				?>

			</div>

	</div>

</form>

<footer>
	<hr>
	<p>Scandidweb Test Assignment</p>
</footer>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    <script type="text/javascript" src="./assets/js/script.js"></script>
</body>
</html>