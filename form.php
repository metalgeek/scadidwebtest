<?php

use TaskApp\DbConnection\DbOperation as DbOperation;

require_once __DIR__ . '/vendor/autoload.php';


extract($_POST);

interface ProductInterface{
    public function process();
}

class Book implements ProductInterface {
    public function process(){

        extract($_POST);

        $objdelete = new DbOperation();
		$data = $objdelete->addProductBook($name,$select,$price,$weight,$sku,$type,$symbol);
    }
}
class Disc implements ProductInterface {
    public function process(){

        extract($_POST);

        $objdelete = new DbOperation();
		$data = $objdelete->addProductDisc($name,$select,$price,$size,$sku,$type,$symbol);
        
    }
}
class Furniture implements ProductInterface {
    public function process(){

        extract($_POST);

        $objdelete = new DbOperation();
		$data = $objdelete->addProductFurniture($name,$select,$price,$length,$width,$height,$sku,$type,$symbol);
        
    }
}

class RunProcess{
    public function processNow(ProductInterface $processType){
        $processType->process();
    }
}


$processType= new $_POST['select'];
$runProcess = new Runprocess();
$runProcess->processNow($processType);

//Alternative instanciation
// $objdelete = new DbOperation();
// $data = new $processType;
// $data == $processType->process();




?>