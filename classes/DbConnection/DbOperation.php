<?php 
namespace TaskApp\DbConnection;

use TaskApp\DbConnection\Connection as Connection;



	
class DbOperation
	{
		
		public function dbSelect($columnsName,$tablesName, $conditions)
		{
			$objConnection = new Connection();
			// object declaretion for using Connection class. Connection class is in Conn.php file
			$objConnection->dbConnection();
			$con = $objConnection->con;
			$result = mysqli_query( $con, "SELECT ".$columnsName." FROM ".$tablesName." WHERE ".$conditions);
			
			return $result;
		}

		public function dbSelectAll($columnsName,$tablesName)
		{
			$objConnection = new Connection();
			// object declaretion for using Connection class. Connection class is in Conn.php file
			$objConnection->dbConnection();
			$con = $objConnection->con;
			$result = mysqli_query( $con, "SELECT ".$columnsName." FROM ".$tablesName);
			
			return $result;
		}

		public function deleteProduct($tablesName,$conditions)
		{
			$objConnection = new Connection();
			// object declaretion for using Connection class. Connection class is in Conn.php file
			$objConnection->dbConnection();
			$con = $objConnection->con;
			$result = mysqli_query( $con, "DELETE FROM ".$tablesName." WHERE  id = '".$conditions."'");
			
			return $result;
		}

		
		public function addProductDisc($name,$category,$price,$size,$sku,$type,$symbol)
		{
			//new db connection instance
			$objConnection = new Connection();
			$objConnection->dbConnection();
			$con = $objConnection->con;


			$result = mysqli_query( $con, "INSERT INTO products (category,name,price,value,sku,type,symbol) VALUES('$category','$name','$price','$size','$sku','$type','$symbol')");
			
			return $result;
		}

		public function addProductFurniture($name,$category,$price,$length,$width,$height,$sku,$type,$symbol)
		{
			//new db connection instance
			$objConnection = new Connection();
			$objConnection->dbConnection();
			$con = $objConnection->con;


			$result = mysqli_query( $con, "INSERT INTO products (category,name,price,value,sku,type,symbol) VALUES('$category','$name','$price','".$height."x".$width."x".$length."','$sku','$type','$symbol')");
			
			return $result;
		}

		public function addProductBook($name,$category,$price,$weight,$sku,$type,$symbol)
		{
			//new db connection instance
			$objConnection = new Connection();
			$objConnection->dbConnection();
			$con = $objConnection->con;


			$result = mysqli_query( $con, "INSERT INTO products (category,name,price,value,sku,type,symbol) VALUES('$category','$name','$price','$weight','$sku','$type','$symbol')");
			
			return $result;
		}

		
	}








?>