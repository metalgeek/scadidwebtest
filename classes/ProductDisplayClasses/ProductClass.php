<?php
namespace TaskApp\ProductDisplayClasses;

use TaskApp\DbConnection\DbOperation as DbOperation;


class ProductClass extends DbOperation
{

		public function getProducts()
		{
			$objDbOperation = new DbOperation();

			$columnsName = "*";
			//database column name
			$tablesName = "products";
			//database table name
			$conditions = "id = id";

			$result = $objDbOperation->dbSelect($columnsName,$tablesName,$conditions);
			return $result;
		}	
	
}


?>
