<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitd2c700a0f48a970ce3652a2218276ec5
{
    public static $prefixLengthsPsr4 = array (
        'T' => 
        array (
            'TaskApp\\' => 8,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'TaskApp\\' => 
        array (
            0 => __DIR__ . '/../..' . '/classes',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
        'TaskApp\\DbConnection\\Connection' => __DIR__ . '/../..' . '/classes/DbConnection/Connection.php',
        'TaskApp\\DbConnection\\DbOperation' => __DIR__ . '/../..' . '/classes/DbConnection/DbOperation.php',
        'TaskApp\\ProductDisplayClasses\\ProductClass' => __DIR__ . '/../..' . '/classes/ProductDisplayClasses/ProductClass.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitd2c700a0f48a970ce3652a2218276ec5::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitd2c700a0f48a970ce3652a2218276ec5::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInitd2c700a0f48a970ce3652a2218276ec5::$classMap;

        }, null, ClassLoader::class);
    }
}
