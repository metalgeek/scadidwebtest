<?php
use TaskApp\ProductDisplayClasses\ProductClass as ProductClass;
require_once __DIR__ . '/vendor/autoload.php';


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="./styles/style.css">
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    <title>Junior Dev. Task</title>
</head>
<body>
<div class="container">
	<form id="product_form">
		<header>
			<div class="row">
				<div class="col-md-8">
					<h3>Add Product</h3>
				</div>
				<div class="col-md-4">
					
					<button class="btn btn-success" id="btnAdd" type="submit">Save</button>
					<button class="btn btn-danger" onclick="IndexPageRedirect()">Cancel</button>
				</div>
			</div>
			<hr>

		</header>
		<div class="container-fluid">
		
		
			<div class="row col-md-6">
				<div class="form-group">
					<label>SKU</label>
					<input type="text" id="sku" name="sku" pattern="[a-zA-Z0-9]+" title="Please enter text or alphanumberic value here" class="form-control" required>
					
				</div>
				<div class="form-group">
					<label>Name</label>
					<input type="text"id="name" name="name" class="form-control" title="Please enter name of poriduct here" required>
				
				</div>
				<div class="form-group">
					<label>Price ($)</label>
	              	<input type="text" id="price" pattern="[0-9]*" title="Please enter product price here" class="form-control" name="price" required />
	            </div>
	
				<div class="form-group">
					<label>Product Category</label>
					<select id="productType" name="select" onchange="selectChange()">
						<option value="TypeSwitcher">Click here...</option>
						<option id="book"  value="Book">Book</option>
						<option id="DVD"  value="Disc">DVD</option>
						<option id="furniture" value="Furniture">Furniture</option>
					</select>
					
				</div>
				<div class="form-group" id="productForm">
					<!-- All forms will be displayed here -->
				</div>


			</div>
			
		
		</div>
	</form>

	</div>
<footer>
	<hr>
	<p>Scandidweb Test Assignment</p>
</footer>




    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    <script type="text/javascript" src="./assets/js/script.js"></script>
</body>
</html>